# Data Wrangling.

## How to run this project.

1. Open this project's module `data_wrangling.ipynb` and select your Kernel. Upon selecting the type of Kernel, the VSCode will let you choose which virtual environments of Python you want, then choose and select your favorable type of environment.

2. Open your terminal, and activate the virtual environment that you selected in the Kernel's settings.

3. Install the dependencies and packages required to run this package. Run the following command:
```
pip install -r requirements.txt
```
- This will install the required modules/packages for this project's package.

4. This project is executed within the Jupyter Notebook environment, which offers an interactive platform for code execution and data analysis. When using Jupyter Notebook, you can readily interact with the output of code cells. If your code generates visualizations, such as plots and tables, they will be directly displayed within the notebook interface.

To run a specific code cell, you can follow these steps:

- Select the cell containing the code you want to execute.
- Press `Shift + Enter` or click the `Run` button located in the toolbar at the top of the notebook. Alternatively, you can run all cells sequentially by clicking the `Run All` button on the toolbar.
- Upon executing a cell, the code within it will be processed, and any output, including results, visualizations, and textual information, will be presented immediately below the executed cell. This interaction allows for a seamless and dynamic exploration of your code and data.

5. If you encounter any issues or want to start fresh, you can restart the kernel using the "Kernel" menu. You can also clear cell output using the "Cell" menu to keep your notebook tidy.